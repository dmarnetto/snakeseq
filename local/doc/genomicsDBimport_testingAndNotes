https://sites.google.com/a/broadinstitute.org/legacy-gatk-forum-discussions/2018-08-10-2018-04-11/12443-GenomicsDBImport-run-slowly-with-multiple-samples

https://gatk.broadinstitute.org/hc/en-us/articles/360056138571-GenomicsDBImport-usage-and-performance-guidelines

https://gatk.broadinstitute.org/hc/en-us/search?utf8=%E2%9C%93&query=GenomicsDBImport+slow


A large number of intervals can cause the import process to be slow, especially if the intervals are small (1,000s or 10,000s of bases). In this case, the import tool will be inefficient because the overhead of opening and closing the gVCF

The sizes of the intervals do not matter so much as the sheer number. The arg called --merge-input-intervals will do the “staging” once, but traverse all the data. In an exome case where HaplotypeCaller was run on the same intervals and there was no data outside those intervals, we found that it worked great.

as the number of contigs starts to approach/exceed 100, you may start to see issues.

There is no fixed number of intervals that is "too many", but anywhere from hundreds thousands might be problematic. We typically work with exomes containing ~70,000 intervals, which have to be run with merged intervals as above.

Importing a large number of samples, and opening many file handles in parallel, can cause increased memory usage. This can cause slowdown, and in the worst case scenario cause the import to crash. Setting --batch-size to something smaller - say 50 will help.

A lot of the heavy lifting for the GenomicsDBImport process is done by underlying C/C++ libraries. If you set Java’s Xmx/Xms options to use up all the available physical memory, these C/C++ libraries will run out of memory and cause failures. While the exact memory usage will depend on the number of samples being imported, we would suggest setting the Java Xmx/Xms values to no more than 80% or 90% of the available physical memory.

--batch-size
controls the number of samples for which readers are open at once and therefore provides a way to minimize memory consumption. However, it can take longer to complete. Use the consolidate flag if more than a hundred batches were used. This will improve feature read time. We like batch size of 50. Larger batches need more memory and file handles, but should run faster.

#################
###  tests:  ####
#################

grep "^chr22" exons.bed | mergeBed > chr22.exons.bed
wc -l chr22.exons.bed
7196 chr22.exons.bed
cat chr22.bed
chr22   1       50818468


# 4 samples
# using exons as windows

gatk --java-options "-Xmx16384m -Xms1024m -XX:ConcGCThreads=1" GenomicsDBImport -V gvcf/FTD-F074.g.vcf -V gvcf/FTD-F109.g.vcf -V gvcf/FTD-F320.g.vcf -V gvcf/FTD-F261.g.vcf --genomicsdb-workspace-path genomics_db --interval-padding 2500 -L chr22.exons.bed 2> genomicsdbimport.test1.log
interrupted > 10 min

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=1" GenomicsDBImport -V gvcf/FTD-F074.g.vcf -V gvcf/FTD-F109.g.vcf -V gvcf/FTD-F320.g.vcf -V gvcf/FTD-F261.g.vcf --genomicsdb-workspace-path genomics_db_test3 --interval-padding 2500 -L chr22.exons.bed --merge-input-intervals 2> genomicsdbimport.test3.log
[December 9, 2021 1:51:28 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.88 minutes.
Runtime.totalMemory()=1900019712


# using entire chromosomes

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=1" GenomicsDBImport -V gvcf/FTD-F074.g.vcf -V gvcf/FTD-F109.g.vcf -V gvcf/FTD-F320.g.vcf -V gvcf/FTD-F261.g.vcf --genomicsdb-workspace-path genomics_db_test2 -L chr22.bed 2> genomicsdbimport.test2.log
[December 9, 2021 1:50:37 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.38 minutes.
Runtime.totalMemory()=1868562432

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=1" GenomicsDBImport -V gvcf/FTD-F074.g.vcf -V gvcf/FTD-F109.g.vcf -V gvcf/FTD-F320.g.vcf -V gvcf/FTD-F261.g.vcf --genomicsdb-workspace-path genomics_db_test4 -L chr1.bed --merge-input-intervals 2> genomicsdbimport.test4.log
[December 9, 2021 2:45:12 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 1.25 minutes.
Runtime.totalMemory()=1612185600

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=2" GenomicsDBImport -V gvcf/FTD-F074.g.vcf -V gvcf/FTD-F109.g.vcf -V gvcf/FTD-F320.g.vcf -V gvcf/FTD-F261.g.vcf --genomicsdb-workspace-path genomics_db_test9 -L chr1.bed 2> genomicsdbimport.test9.log
[December 9, 2021 4:28:32 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 1.24 minutes.
Runtime.totalMemory()=1603796992


# rerun test2 with different memory options

VIRT	Xmx	remaining
4982M	2000	2982
[December 9, 2021 2:06:34 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.32 minutes.
Runtime.totalMemory()=1333264384

5032M	2048	2984
[December 9, 2021 2:03:58 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.32 minutes.
Runtime.totalMemory()=1351090176

7155M	4096	3059
[December 9, 2021 2:00:40 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.32 minutes.
Runtime.totalMemory()=1789394944

7155M	4096	3059 ->-Xms 4096
[December 9, 2021 3:53:00 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.35 minutes.
Runtime.totalMemory()=4116709376

	64
[December 9, 2021 2:24:29 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 0.05 minutes.
Runtime.totalMemory()=59768832
java.util.concurrent.CompletionException: java.lang.OutOfMemoryError: Java heap space


# all chromosomes

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=2" GenomicsDBImport -V gvcf/FTD-F187.g.vcf ... -V gvcf/FTD-F072.g.vcf --genomicsdb-workspace-path genomics_db_test5 -L chr22.bed --batch-size 50 2> genomicsdbimport.test5.log
[December 9, 2021 3:05:50 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 7.63 minutes.
Runtime.totalMemory()=5163712512

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=2" GenomicsDBImport -V gvcf/FTD-F187.g.vcf ... -V gvcf/FTD-F072.g.vcf --genomicsdb-workspace-path genomics_db_test6 -L chr1.bed --batch-size 50 2> genomicsdbimport.test6.log
virt 11.3G res 3491
[December 9, 2021 3:30:50 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 32.81 minutes.
Runtime.totalMemory()=3449815040

gatk --java-options "-Xmx16000m -Xms1024m -XX:ConcGCThreads=2" GenomicsDBImport -V gvcf/FTD-F187.g.vcf ... -V gvcf/FTD-F072.g.vcf --genomicsdb-workspace-path genomics_db_test7 -L chr1.bed --batch-size 50 --reader-threads 2 2> genomicsdbimport.test7.log
[December 9, 2021 4:08:55 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 32.60 minutes.
Runtime.totalMemory()=4520411136

gatk --java-options "-Xmx12000m -Xms1024m -XX:ConcGCThreads=2" GenomicsDBImport -V gvcf/FTD-F187.g.vcf ... -V gvcf/FTD-F072.g.vcf --genomicsdb-workspace-path genomics_db_test8 -L chr1.bed 2> genomicsdbimport.test8.log
[December 9, 2021 4:10:07 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 31.52 minutes.
Runtime.totalMemory()=6754402304

gatk --java-options "-Xmx8192m -Xms1024m -XX:ConcGCThreads=2" GenomicsDBImport -V gvcf/FTD-F187.g.vcf ... -V gvcf/FTD-F072.g.vcf --genomicsdb-workspace-path genomics_db_test10 -L chr22.bed 2> genomicsdbimport.test10.log
[December 9, 2021 4:56:37 PM CET] org.broadinstitute.hellbender.tools.genomicsdb.GenomicsDBImport done. Elapsed time: 7.72 minutes.
Runtime.totalMemory()=4902092800
